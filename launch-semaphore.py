#! /usr/bin/env python3

import argparse
import datetime
import getpass
import os
import requests
import time
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count
from requests.compat import urljoin

from junit_xml import TestSuite, TestCase, to_xml_report_file

class SemaphoreLauncher:

    API_HEADERS = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    }

    def start_task(self, template_id):

        start_url = urljoin(self.url_base, "api/project/{}/tasks".format(
            self.project_id,
        ))

        start_req = self.session.post(
            start_url,
            headers=self.API_HEADERS,
            json={
                "template_id": template_id,
                "debug": self.debug,
                "dry_run": self.dry_run,
            }
        )

        if start_req.status_code != 201:
            raise RuntimeError("Start task call failed with code {}. {}".format(
                start_req.status_code,
                start_req.text,
            ))

        task_id = start_req.json().get("id")
        return task_id


    def check_task_status(self, task_id: int):


        task_url = urljoin(
            self.url_base,
            "api/project/{proj_id}/tasks/{task_id}".format(
                proj_id=self.project_id,
                task_id=task_id,
            ),
        )

        task_req = self.session.get(
            task_url,
            headers=self.API_HEADERS,
        )

        task_req.raise_for_status()

        return task_req.json()

    def get_task_output(self, task_id: int):

        url = urljoin(
            self.url_base,
            "api/project/{proj_id}/tasks/{task_id}/output".format(
                proj_id=self.project_id,
                task_id=task_id,
            ),
        )

        req = self.session.get(
            url,
            headers=self.API_HEADERS,
        )

        req.raise_for_status()
        req_data = req.json()

        fmt_output = map(
            lambda x: "[{time}] {line}".format(
                time=x.get('time').ljust(27, ' '),
                line=x.get('output'),
            ),
            req_data,
        )

        def get_t(x):
            return datetime.datetime.fromisoformat(x.get('time').rstrip('Z').ljust(26, '0'))
        elapsed_sec = get_t(req_data[-1]) - get_t(req_data[0])


        return {
            "stdout": os.linesep.join(fmt_output),
            "elapsed_sec": elapsed_sec.total_seconds(),
        }

    def __store_template_info(self):

        templates_url = urljoin(
            self.url_base,
            "api/project/{}/templates".format(self.project_id),
        )

        tmp_req = self.session.get(
            templates_url,
            headers=self.API_HEADERS,
        )

        tmp_req.raise_for_status()

        self.template_info = tmp_req.json()


    def get_template_by_name(self, template_name: str):

        templates = list(filter(
            lambda x: x.get('alias') == template_name,
            self.template_info,
        ))

        if len(templates) == 0:
            return None
        
        if len(templates) > 1:
            raise RuntimeError("More than one template returned")

        return templates[0].get('id')

    def __init__(
        self,
        url_base: str,
        username: str,
        password: str,
        project_id: int,
        dry_run=False,
        debug=False,
    ):

        self.url_base = url_base
        self.project_id = project_id
        self.dry_run = dry_run
        self.debug = debug

        self.session = requests.Session()

        auth_login = self.session.post(
            urljoin(self.url_base, "api/auth/login"),
            json={
                "auth": username,
                "password": password,
            },
            headers=self.API_HEADERS,
        )

        if auth_login.status_code != 204:
            raise RuntimeError("Login failed with status code: {}".format(auth_login.status_code))

        self.__store_template_info()

    def wait_for_task_by_name(self, template_name: str):

        template_id = self.get_template_by_name(template_name)
        task_id = self.start_task(template_id)

        while True:
            time.sleep(1)
            task_status = self.check_task_status(task_id)
            if task_status.get("end"):
                break

        task_status['alias'] = template_name
        task_output = self.get_task_output(task_id)
        task_status['output'] = task_output.get('stdout')
        task_status['elapsed_sec'] = task_output.get('elapsed_sec')
        return task_status



class XunitWriter:
    # https://help.catchsoftware.com/display/ET/JUnit+Format
    # https://github.com/kyrus/python-junit-xml

    def add_result(self, task_status):

        tc = TestCase(
            task_status.get('alias'),
            status=task_status.get('status'),
            classname=task_status.get('playbook'),
            stdout=task_status.get('output'),
            elapsed_sec=task_status.get('elapsed_sec'),
        )

        if task_status.get('status') != 'success':
            tc.add_failure_info(failure_type=task_status.get('status'))

        return tc

    def add_all_results(self, tasks_status):
        """ add all results to the tree """
        self.testCases = [self.add_result(x) for x in tasks_status]


    def write_xml(self, suite_name, file_path):

        ts = TestSuite(suite_name, self.testCases)
        with open(file_path, 'w') as f:
            to_xml_report_file(f, [ts])



if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "template_alias", nargs="+", help="semaphore template aliases",
    )

    parser.add_argument(
        "--threads", type=int, dest="threads",
        default=max(1, cpu_count()-1),
        help="Number of executor threads.",
    )

    parser.add_argument(
        "--xunit-file", type=str, dest="xunit_file",
        default=os.getenv("XUNIT_FILE_PATH"),
        help="path to write xunit file",
    )

    env_args = parser.add_argument_group("Normally set by environment")

    env_args.add_argument(
        "--url-base", dest="url_base", type=str,
        default=os.getenv("SEMAPHORE_URL_BASE"),
        help="Semaphore base url - set with env variable SEMAPHORE_URL_BASE",
    )

    env_args.add_argument(
        "--username", dest="semaphore_username", type=str,
        default=os.getenv("SEMAPHORE_USERNAME"),
        help="Semaphore username - set with env variable SEMAPHORE_USERNAME",
    )

    env_args.add_argument(
        "--prompt-for-password", dest="prompt_for_password",
        default=False, action="store_true",
        help="Will prompt for semaphore password - set with env variable SEMAPHORE_PASSWORD",
    )

    env_args.add_argument(
        "--project-id", dest="semaphore_proj_id", type=int,
        default=int(os.getenv("SEMAPHORE_PROJECT_ID", 1)),
        help="Set semaphore project id, default 1 - also set with env variable SEMAPHORE_PROJECT_ID",
    )

    debug_args = parser.add_argument_group("Debug Arguments")

    debug_args.add_argument(
        "--verbose", dest="debug", action="store_true", default=False,
        help="Use -vvvv argument with ansible",
    )

    debug_args.add_argument(
        "--dry-run", action="store_true", default=False,
        help="Use Ansible dry run",
    )

    args = parser.parse_args()

    if args.prompt_for_password:
        semaphore_pass = getpass.getpass("Semaphore Password: ")
    elif os.getenv("SEMAPHORE_PASSWORD"):
        semaphore_pass = os.getenv("SEMAPHORE_PASSWORD")
    else:
        raise RuntimeError("No semaphore password passed.")

    s = SemaphoreLauncher(
        args.url_base,
        args.semaphore_username,
        semaphore_pass,
        args.semaphore_proj_id,
        dry_run=args.dry_run,
        debug=args.debug,
    )

    worker_count = min(args.threads, len(args.template_alias))
    with ThreadPoolExecutor(max_workers=worker_count) as executor:
        task_status = list(executor.map(
            s.wait_for_task_by_name,
            args.template_alias,
        ))

    completed_tasks = list(filter(lambda x: isinstance(x, dict), task_status))

    for i in completed_tasks:
        print("Task {t} completed. Name: {n}, Status: {s}".format(
            t=i.get('id'),
            n=i.get('alias'),
            s=i.get('status'),
        ))

    if i in filter(lambda x: isinstance(x, Exception), task_status):
        print(i)

    if args.xunit_file:
        xunit = XunitWriter()
        xunit.add_all_results(completed_tasks)
        xunit.write_xml("semaphore results", args.xunit_file)
