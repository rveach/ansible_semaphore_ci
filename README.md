# launch-semaphore

script useful for launching semaphore tasks.

see https://github.com/ansible-semaphore

```
$ launch-semaphore.py -h
usage: launch-semaphore.py [-h] [--url-base URL_BASE]
                           [--username SEMAPHORE_USERNAME]
                           [--prompt-for-password]
                           [--project-id SEMAPHORE_PROJ_ID] [--verbose]
                           [--dry-run]
                           template_alias

positional arguments:
  template_alias        semaphore template alias

optional arguments:
  -h, --help            show this help message and exit

Normally set by environment:
  --url-base URL_BASE   Semaphore base url - set with env variable
                        SEMAPHORE_URL_BASE
  --username SEMAPHORE_USERNAME
                        Semaphore username - set with env variable
                        SEMAPHORE_USERNAME
  --prompt-for-password
                        Will prompt for semaphore password - set with env
                        variable SEMAPHORE_PASSWORD
  --project-id SEMAPHORE_PROJ_ID
                        Set semaphore project id, default 1 - also set with
                        env variable SEMAPHORE_PROJECT_ID

Debug Arguments:
  --verbose             Use -vvvv argument with ansible
  --dry-run             Use Ansible dry run
```
